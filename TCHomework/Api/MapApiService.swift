//
//  MapApiService.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation

public enum MapApiService {
    case googleMapDirection(from: OnMapLocation, to: OnMapLocation, mode: TransportMode)
}
