//
//  MapApiService+Moya.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import Moya

extension MapApiService: TargetType {
    public var baseURL: URL {
        return URL(string: Config.GOOGLE_DIRECTION_BASE_URL)!
    }
    
    public var method: Moya.Method {
        switch self {
        case .googleMapDirection(_, _, _):
            return .GET
        }
    }
    
    public var task: Task {
        switch self {
        default:
            return .request
        }
    }
    
    public var sampleData: Data {
        switch self {
        default:
            return "No sample data yet".data(using: String.Encoding.utf8)!
        }
    }
    
    public var path: String {
        switch self {
        case .googleMapDirection(_, _, _):
            return "/maps/api/directions/json"
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .googleMapDirection(let from, let to, let mode):
            let param = [
                "origin": "\(from.coordinate!.latitude),\(from.coordinate!.longitude)",
                "destination": "\(to.coordinate!.latitude),\(to.coordinate!.longitude)",
                "key": "\(Config.GOOGLE_MAP_API_KEY)",
                "mode": "\(mode.rawValue)"
            ]
            return param
        }
    }
}
