//
//  MapViewController.swift
//  TCHomework
//
//  Created by Huy Pham on 10/5/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import RxSwift
import RxCocoa

public class MapViewController: UIViewController, CLLocationManagerDelegate, GMSAutocompleteResultsViewControllerDelegate, UISearchControllerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var buttonA: TriStateButton!
    @IBOutlet weak var buttonB: TriStateButton!
    @IBOutlet weak var transportMethodSelector: TransportMethodSelector!
    
    private let locationManager = CLLocationManager()
    private var disposeBag = DisposeBag()
    private var searchController: UISearchController?
    private var markerA = GMSMarker()
    private var markerB = GMSMarker()
    private var viewModel = MapViewModel()
    private var routeLine: GMSPolyline?
    private var searchBarWrapper: UIView!
    
    // MARK: - Lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        requestAccessLocationPermission()
        focusMapOn(targetCoordinate: nil)
        prepareSearchResultViewController()
        bindViewModel()
    }
    
    public func willDismissSearchController(_ searchController: UISearchController) {
        searchBarWrapper.isHidden = true
    }
    
    // MARK: Google Places delegates
    
    public func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        
        switch viewModel.locationSelectMode {
        case .SelectingA:
            viewModel.locationA = OnMapLocation(name: place.name, coordinate: place.coordinate)
        case .SelectingB:
            viewModel.locationB = OnMapLocation(name: place.name, coordinate: place.coordinate)
        default:
            break
        }
    }
    
    public func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        UiUtils.showAlert(self, title: "Error", message: error.localizedDescription, completion: nil)
    }
    
    // MARK: Location Manager
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        default:
            break
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            focusMapOn(targetCoordinate: location.coordinate)
            locationManager.stopUpdatingLocation()
        }
    }
    
    // MARK: User's actions
    
    @IBAction func selectDepartLocation(_ sender: AnyObject) {
        viewModel.locationSelectMode = .SelectingA
        focusOnSearchBar()
    }
    
    @IBAction func selectDestinationLocation(_ sender: AnyObject) {
        viewModel.locationSelectMode = .SelectingB
        focusOnSearchBar()
    }
    
    @IBAction func swapLocations(_ sender: AnyObject) {
        viewModel.swapLocations()
    }
    
    // MARK: Helpers
    
    private func requestAccessLocationPermission() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
    private func focusMapOn(targetCoordinate: CLLocationCoordinate2D?, animated: Bool = false) {
        let targetLoc = targetCoordinate ?? CLLocationCoordinate2D(latitude: Config.MAP_LNG_FALLBACK_HCMC, longitude: Config.MAP_LAT_FALLBACK_HCMC)
        if animated {
            mapView.animate(to: GMSCameraPosition(target: targetLoc, zoom: 15, bearing: 0, viewingAngle: 0))
        } else {
            mapView.camera = GMSCameraPosition(target: targetLoc, zoom: 15, bearing: 0, viewingAngle: 0)
        }
    }
    
    private func prepareSearchResultViewController() {
        let resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        searchController!.delegate = self
        
        searchBarWrapper = UIView(frame: CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 45))
        searchBarWrapper.addSubview((searchController?.searchBar)!)
        self.view.addSubview(searchBarWrapper)
        searchBarWrapper.isHidden = true
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
        self.definesPresentationContext = true
    }
    
    private func focusOnSearchBar() {
        searchBarWrapper?.isHidden = false
        searchController?.searchBar.becomeFirstResponder()
    }
    
    private func bindViewModel() {
        viewModel.rxLocationA.asObservable()
            .skip(1)
            .subscribe(onNext: { location in
                    self.updateMapAfterSelected(location: location, byPressing: self.buttonA, andShow: &self.markerA)
                }, onError: nil, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        viewModel.rxLocationB.asObservable()
            .skip(1)
            .subscribe(onNext: { location in
                    self.updateMapAfterSelected(location: location, byPressing: self.buttonB, andShow: &self.markerB)
                }, onError: nil, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        viewModel.rxTransportMode.asObservable()
            .skip(1)
            .subscribe(onNext: { _ in
                    self.showRouteIfAvailable(lastSelectedLocation: nil)
                }, onError: nil, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        transportMethodSelector.rxTransportMode.asObservable()
            .subscribe(onNext: { transportMethod in
                    self.viewModel.transportMode = transportMethod
                }, onError: nil, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        viewModel.rxMapDirectionResult.asObservable()
            .subscribe(onNext: { directionResult in
                    self.drawRoute(direction: directionResult)
                }, onError: nil, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
        
        viewModel.rxErrorMessage.asObservable()
            .subscribe(onNext: { errorMessage in
                    UiUtils.showAlert(self, title: errorMessage?.title, message: errorMessage?.message)
                }, onError: nil, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
    }
    
    private func updateMapAfterSelected(location: OnMapLocation?, byPressing button: TriStateButton, andShow marker: inout GMSMarker) {
        if let location = location {
            setMarker(marker: &marker, toLocation: location, withColor: button.labelColor)
        } else {
            removeMarker(marker: marker)
        }
        
        button.selectionState = (location == nil ? .None : .Selected)
        viewModel.locationSelectMode = .None
        showRouteIfAvailable(lastSelectedLocation: location)
    }
    
    private func setMarker(marker: inout GMSMarker, toLocation location: OnMapLocation, withColor color: UIColor) {
        marker.map = nil
        marker = GMSMarker(position: location.coordinate!)
        marker.title = location.name
        marker.icon = GMSMarker.markerImage(with: color)
        marker.map = mapView
    }
    
    private func removeMarker(marker: GMSMarker) {
        marker.map = nil
    }
    
    private func showRouteIfAvailable(lastSelectedLocation: OnMapLocation?) {
        if viewModel.isAllLocationSet() {
            viewModel.findRoute()
        } else {
            if let lastSelectedLocation = lastSelectedLocation {
                focusMapOn(targetCoordinate: lastSelectedLocation.coordinate, animated: true)
            }
        }
    }
    
    private func drawRoute(direction: MapDirectionResult?) {
        // Remove old route
        if let oldRoute = routeLine {
            oldRoute.map = nil
        }
        
        // Check if there is a route
        guard let direction = direction, let routes = direction.routes else {
            return
        }
        if routes.count == 0 {
            viewModel.errorMessage = ErrorMessage(title: "Sorry", message: "There is no route!")
            return
        }
        let steps = direction.routes![0].legs![0].steps!
        
        // Connect the points along the path
        let path = GMSMutablePath()
        for step in steps {
            path.add(step.startLocation!.toCoordinate2D())
            path.add(step.endLocation!.toCoordinate2D())
        }
        
        // Draw route
        routeLine = GMSPolyline(path: path)
        routeLine!.map = mapView
        
        // Animate map to fit the route in screen
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(direction.routes![0].northEast!.toCoordinate2D())
        bounds = bounds.includingCoordinate(direction.routes![0].southWest!.toCoordinate2D())
        mapView.animate(with: GMSCameraUpdate.fit(bounds))
    }
}
