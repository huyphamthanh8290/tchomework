//
//  MapViewModel.swift
//  TCHomework
//
//  Created by Huy Pham on 10/7/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import ObjectMapper
import GoogleMaps

public class MapViewModel {
    public var locationA: OnMapLocation? {
        didSet {
            rxLocationA.value = locationA
        }
    }
    public var locationB: OnMapLocation? {
        didSet {
            rxLocationB.value = locationB
        }
    }
    public var transportMode = TransportMode.Driving {
        didSet {
            rxTransportMode.value = transportMode
        }
    }
    public var locationSelectMode = LocationSelectMode.None
    public var mapDirectionResult: MapDirectionResult? {
        didSet {
            rxMapDirectionResult.value = mapDirectionResult
        }
    }
    public var errorMessage: ErrorMessage? {
        didSet {
            rxErrorMessage.value = errorMessage
        }
    }
    
    public var rxLocationA = Variable<OnMapLocation?>(OnMapLocation())
    public var rxLocationB = Variable<OnMapLocation?>(OnMapLocation())
    public var rxTransportMode = Variable<TransportMode>(.Driving)
    public var rxMapDirectionResult = Variable<MapDirectionResult?>(nil)
    public var rxErrorMessage = Variable<ErrorMessage?>(nil)
    public var apiProvider = MoyaProvider<MapApiService>()
    
    // MARK: -
    
    public init() {
        
    }
    
    public func isAllLocationSet() -> Bool {
        return locationA != nil && locationB != nil
    }
    
    public func swapLocations() {
        let tmpA = locationA
        let tmpB = locationB
        
        locationA = nil
        locationB = nil
        locationB = tmpA
        locationA = tmpB
    }
    
    public func findRoute() {
        guard let fromLocation = locationA, let toLocation = locationB else {
            return
        }
        
        apiProvider.request(.googleMapDirection(from: fromLocation, to: toLocation, mode: transportMode)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200:
                    do {
                        let mapDirection = Mapper<MapDirectionResult>().map(JSONString: try response.mapString())
                        if let mapDirection = mapDirection {
                            self.mapDirectionResult = mapDirection
                        } else {
                            self.errorMessage = ErrorMessage(title: "Sorry", message: "There is no route from \(fromLocation.name!) to \(toLocation.name!)")
                        }
                    } catch {
                        self.errorMessage = ErrorMessage(title: "Error", message: "Oops, something went wrong! Please try again later.")
                    }
                default:
                    self.errorMessage = ErrorMessage(title: "Error", message: "Error getting path from Google Map!")
                }
            case let .failure(error):
                self.errorMessage = ErrorMessage(title: "Error", message: error.localizedDescription)
            }
        }
    }
}
