//
//  ErrorMessage.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation

public class ErrorMessage {
    var title: String?
    var message: String?
    
    public init() {
    }
    
    public init(title: String, message: String) {
        self.title = title
        self.message = message
    }
}
