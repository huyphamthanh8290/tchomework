//
//  OnMapLocation.swift
//  TCHomework
//
//  Created by Huy Pham on 10/7/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import GoogleMaps

public class OnMapLocation {
    var name: String?
    var coordinate: CLLocationCoordinate2D?
    
    public init() {
    }
    
    public init(name: String, coordinate: CLLocationCoordinate2D) {
        self.name = name
        self.coordinate = coordinate
    }
}
