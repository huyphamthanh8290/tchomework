//
//  MapDirectionResult.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import ObjectMapper

public class MapDirectionResult: Mappable {
    var routes: [MapRoute]?
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        routes <- map["routes"]
    }
}
