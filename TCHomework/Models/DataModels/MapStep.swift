//
//  MapStep.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import ObjectMapper

public class MapStep: Mappable {
    var startLocation: MapCoordinate?
    var endLocation: MapCoordinate?
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        startLocation   <- map["start_location"]
        endLocation     <- map["end_location"]
    }
}
