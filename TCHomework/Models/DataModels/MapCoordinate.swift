//
//  MapCoordinate.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import ObjectMapper
import GoogleMaps

public class MapCoordinate: Mappable {
    var lat: Double?
    var lng: Double?
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
    
    func toCoordinate2D() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: lat!, longitude: lng!)
    }
}
