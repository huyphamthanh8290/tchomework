//
//  MapLeg.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import ObjectMapper

public class MapLeg: Mappable {
    var steps: [MapStep]?
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        steps <- map["steps"]
    }
}
