//
//  LocationSelectMode.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation

public enum LocationSelectMode {
    case None
    case SelectingA
    case SelectingB
}

