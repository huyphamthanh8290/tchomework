//
//  MapRoute.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import ObjectMapper

public class MapRoute: Mappable {
    var legs: [MapLeg]?
    var northEast: MapCoordinate?
    var southWest: MapCoordinate?
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        legs        <- map["legs"]
        northEast   <- map["bounds.northeast"]
        southWest   <- map["bounds.southwest"]
    }
}
