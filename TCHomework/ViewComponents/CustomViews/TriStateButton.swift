//
//  TriStateButton.swift
//  TCHomework
//
//  Created by Huy Pham on 10/6/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import UIKit
import HPXibDesignable
import HPUIViewExtensions
import RxSwift

@IBDesignable
class TriStateButton: HPControlXibDesignable {
    
    enum SelectionState {
        case None
        case Selected
    }
    
    // MARK: -
    
    @IBOutlet weak var innerButton: HPButton!
    
    @IBInspectable var label: String? {
        didSet {
            innerButton.setTitle(label, for: .normal)
        }
    }
    @IBInspectable var labelColor: UIColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) {
        didSet {
            self.innerButton.borderColor = labelColor
            self.innerButton.bgColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.innerButton.setTitleColor(labelColor, for: .normal)
        }
    }
    public var selectionState = SelectionState.None {
        didSet {
            rxSelectionState.value = selectionState
        }
    }
    private var rxSelectionState = Variable<SelectionState>(.None)
    private var disposeBag = DisposeBag()
    
    // MARK: -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        bindUiToState()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        bindUiToState()
    }
    
    @IBAction func buttonDidPress(_ sender: AnyObject) {
        sendActions(for: UIControlEvents.touchUpInside)
    }
    
    private func bindUiToState() {
        rxSelectionState.asObservable()
            .subscribe(onNext: { state in
                switch state {
                case .None:
                    self.innerButton.borderColor = self.labelColor
                    self.innerButton.bgColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    self.innerButton.setTitleColor(self.labelColor, for: .normal)
                case .Selected:
                    self.innerButton.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                    self.innerButton.bgColor = self.labelColor
                    self.innerButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                }
            }, onError: nil, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(disposeBag)
    }
}
