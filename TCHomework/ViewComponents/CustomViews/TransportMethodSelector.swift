//
//  TransportationMethodSelector.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import Foundation
import HPXibDesignable
import RxSwift
import HPUIViewExtensions

@IBDesignable
class TransportMethodSelector: HPXibDesignable {
    @IBOutlet weak var drivingButton: HPButton!
    @IBOutlet weak var walkingButton: HPButton!
    @IBOutlet weak var transitButton: HPButton!
    
    var transportMode = TransportMode.Driving {
        didSet {
            rxTransportMode.value = transportMode
        }
    }
    var rxTransportMode = Variable<TransportMode>(.Driving)
    
    // MARK: -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUI()
    }
    
    private func initUI() {
        drivingButton.bgColor = #colorLiteral(red: 0.2117647059, green: 0.4156862745, blue: 0.8274509804, alpha: 1)
    }
    
    @IBAction func transportMethodSelected(_ sender: AnyObject) {
        drivingButton.bgColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        walkingButton.bgColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        transitButton.bgColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        if let pressedButton = sender as? HPButton {
            pressedButton.bgColor = #colorLiteral(red: 0.2117647059, green: 0.4156862745, blue: 0.8274509804, alpha: 1)
            
            switch pressedButton {
            case drivingButton:
                transportMode = .Driving
            case walkingButton:
                transportMode = .Walking
            case transitButton:
                transportMode = .Transit
            default:
                break
            }
        }
    }
}
