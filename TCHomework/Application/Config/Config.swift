//
//  Config.swift
//  MoolahSwap
//
//  Created by Huy Pham on 9/23/15.
//  Copyright © 2015 Robust Tech House. All rights reserved.
//

import UIKit

class Config {
    static let GOOGLE_MAP_API_KEY           = "AIzaSyBZnAH8ZVXgeFrndwSNVuqxwY3BNxgLsYs"
    static let MAP_LNG_FALLBACK_HCMC        = 10.762622
    static let MAP_LAT_FALLBACK_HCMC        = 106.660172
    static let GOOGLE_DIRECTION_BASE_URL    = "https://maps.googleapis.com"
}
