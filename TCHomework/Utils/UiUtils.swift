//
//  File.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import UIKit

class UiUtils {
    class func showAlert(_ vc: UIViewController, title: String?, message: String?, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        vc.present(alert, animated: true, completion: completion)
    }
}
