//
//  MapViewControllerTest.swift
//  TCHomework
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import XCTest
import UIKit
import TCHomework
import GoogleMaps
import GooglePlaces

class MapViewControllerTest: XCTestCase {
    
    var viewController: MapViewController!
    
    // MARK: - Setup
    
    override func setUp() {
        super.setUp()
        
        viewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Tests
    
}
