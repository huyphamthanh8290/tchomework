//
//  TCHomeworkTests.swift
//  TCHomeworkTests
//
//  Created by Huy Pham on 10/8/16.
//  Copyright © 2016 TrustCircle. All rights reserved.
//

import XCTest
import UIKit
import TCHomework
import Moya

class MapViewModelTests: XCTestCase {
    
    var viewModel: MapViewModel!
    
    // MARK: - Setup
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Tests
    
    func testIsAllLocationSet_AllNotSet() {
        viewModel = MapViewModel()
        
        let result = viewModel.isAllLocationSet()
        
        XCTAssertEqual(result, false)
    }
    
    func testIsAllLocationSet_OneNotSet() {
        viewModel = MapViewModel()
        viewModel.locationA = OnMapLocation()
        
        let result = viewModel.isAllLocationSet()
        
        XCTAssertEqual(result, false)
    }
    
    func testIsAllLocationSet_AllSet() {
        viewModel = MapViewModel()
        viewModel.locationA = OnMapLocation()
        viewModel.locationB = OnMapLocation()
        
        let result = viewModel.isAllLocationSet()
        
        XCTAssertEqual(result, true)
    }
    
    func testSwapLocation() {
        viewModel = MapViewModel()
        viewModel.locationA = OnMapLocation()
        viewModel.locationB = OnMapLocation()
        let oldA = viewModel.locationA
        let oldB = viewModel.locationB
        
        viewModel.swapLocations()
        
        XCTAssert(viewModel.locationA === oldB)
        XCTAssert(viewModel.locationB === oldA)
    }
    
    func testFindRoute() {
        let notFound401EndpointClosure = { (target: MapApiService) -> Endpoint<MapApiService> in
            let url = target.baseURL.appendingPathComponent(target.path).absoluteString
            let endpoint = Endpoint<MapApiService>(URL: url, sampleResponseClosure: {
                    return .networkResponse(401, target.sampleData)
                }, method: target.method, parameters: target.parameters)
//            let headers = headersForTarget(target)
            return endpoint
        }
        let mockProvider = MoyaProvider<MapApiService>(endpointClosure: notFound401EndpointClosure)
        viewModel = MapViewModel()
        viewModel.apiProvider = mockProvider
        
        viewModel.findRoute()
        XCTAssertEqual(viewModel.mapDirectionResult == nil, true)
    }
}
